module.exports = {
  "apps" : [
    {
      "name" : "mgold_service" ,
      "script" : "./index.js" ,
      env : {
        NODE_ENV : "production" ,
        PORT : 1010 ,
      }
    }
  ] ,
  deploy : {
    // "production" is the environment name
    production : {
      // SSH key path, default to $HOME/.ssh
      key : "/home/smarttech/.ssh/id_rsa" ,
      // SSH currentUserGql
      user : "root" ,
      // SSH host
      host : [ "144.202.13.156" ] ,
      // SSH options with no command-line flag, see 'man ssh'
      // can be either a single string or an array of strings
      ssh_options : "StrictHostKeyChecking=no" ,
      // GIT remote/branch
      ref : "origin/master" ,
      // GIT remote
      repo : "git@gitlab.com:davinchipat/mgolg_services.git" ,
      // path in the server
      path : "/root/mgold_service" ,
      // Pre-setup command or path to a script on your local machine
      // 'pre-setup': "cd ~/My_projects/App/cloudBooks_fronend && git push origin nstaging",
      // Post-setup commands or path to a script on the host machine
      // eg: placing configurations in the shared dir etc
      'post-setup' : "npm install; yarn build; pm2 start ecosystem.config.js" ,
      // pre-deploy action
      // 'pre-deploy-local': "echo 'This is a local executed command'",
      // post-deploy action
      'post-deploy' : "npm install; yarn build; pm2 stop mgold_service; pm2 delete mgold_service; pm2 start ecosystem.config.js"
    }
  }
};

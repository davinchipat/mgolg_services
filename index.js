const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const Caterer = require("./schema/index");
const flash = require("connect-flash");
const session = require("express-session");
const cors = require("cors");

mongoose
  .connect("mongodb://localhost:27017/mgold_caterers", { useNewUrlParser: true })
  .then(() => console.log("connect"))
  .catch(() => console.log("could not connct!"));

const app = express();
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.set("views", __dirname + "/views");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  session({
    cookie: { maxAge: 60000 },
    saveUninitialized: true,
    resave: "true",
    secret: "secret"
  })
);
app.use(flash());
app.use(function(req, res, next) {
  res.locals.message = req.flash();
  next();
});

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/client", (req, res) => {
  res.render("regForm");
});

app.post("/add_caterer", async (req, res) => {
  const {
    fullName,
    phone,
    email,
    company,
    location,
    staffNo,
    experience
  } = req.body;
  const existing = await Caterer.findOne({ phone: req.body.phone });
  console.log(existing);
  if (existing) {
    req.flash("error", "Caterer with this phone number has been registered");
   return  res.redirect("/client");
  }
  if (req.body.pin !== "1960") {
    req.flash("error", "Incorrect pin");
    return res.redirect("/client");
  }

  Caterer.create({
    fullName: fullName,
    phone: phone,
    email: email,
    company: company,
    location: location,
    staffNo: staffNo,
    experience: experience
  })
    .then(() => {
      console.log("saved");
      req.flash("success", "Caterer Successfully registered");
      return res.redirect("/");
    })
    .catch(error => {
      req.flash(
        "error",
        "Error registering caterer, Please check data provided"
      );
      res.redirect("/client");
    });
});

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`Running on ${port}`));

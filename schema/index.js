const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const mySchema = new Schema({
  fullName: String,
  phone: String,
  email: String,
  company: String,
  location: String,
  staffNo: Number,
  experience: String
});

const CatModel = mongoose.model("caterer", mySchema);

module.exports = CatModel;
